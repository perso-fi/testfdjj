import { ApiProperty } from "@nestjs/swagger"
import { Team } from "src/team/team.schema"


export class LeagueDto {
    @ApiProperty()
    name: string

    @ApiProperty()
    sport: string
    
    @ApiProperty()
    teams: Team[]
}