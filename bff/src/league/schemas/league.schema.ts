import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import { HydratedDocument, Schema as SchemaMongoose } from 'mongoose'
import { ApiProperty } from "@nestjs/swagger";
import { Team } from "src/team/team.schema";
import { Type } from 'class-transformer'


export type LeagueDocument = HydratedDocument<League>;

@Schema()
export class League {
   @ApiProperty()
   @Prop({unique: true})
   name: string;

   @ApiProperty()
   @Prop({unique: true})
   sport: string;

   @ApiProperty()
   @Prop({type: [{ type: SchemaMongoose.Types.ObjectId, ref: 'Team' }]})
   // @Type(() => Team[])
   teams: Team[];
   // teams: SchemaMongoose.Types.ObjectId;
}
export const LeagueSchema = SchemaFactory.createForClass(League);