import { Controller, Get, HttpStatus, Param, Res } from "@nestjs/common";
import { LeagueService } from "./league.service";


@Controller('leagues')
export class LeagueController {
   constructor(private readonly leagueService: LeagueService) { 

       
    }

    @Get()
    async getLeagues(@Res() response) {
        try {
            const leagues = await this.leagueService.findAllLeague();
            return response.status(HttpStatus.OK).json({leagues});
        } catch (err) {
            return response.status(err.status).json(err.response);
        }
    }

    @Get('/:id')
    async getLeague(@Res() response, @Param('id') leagueId: string) {
        // const existingLeague = await this.leagueService
    }
}
