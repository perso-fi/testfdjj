import { Connection, Model } from 'mongoose';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { League, LeagueDocument} from './schemas/league.schema';
import { LeagueDto } from './league.dto';

@Injectable()
export class LeagueService {
  constructor(
    @InjectModel(League.name) private leagueModel: Model<LeagueDocument>,
    @InjectConnection() private connection: Connection
    ) {}

  

  async findAllLeague(): Promise<League[]> {
    try {
      return this.leagueModel.find().populate('teams');;
    } catch (error) {
      console.log(error)
    }
    
  }

  async findLeague(id: string): Promise<League> {
    const existingLeague = this.leagueModel.findById(id);
    if (!existingLeague) {
      throw new NotFoundException(`League #${id} not found`);
    }
    return existingLeague
  }
}
