import { Document } from "mongoose";
import { Team } from "src/team/team.schema";

export interface LeagueDocument extends Document{
    
    name: string,
    sport: string,
    teams: Team[]
}