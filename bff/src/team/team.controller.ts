import { Controller, Get, HttpStatus, Param, Res } from "@nestjs/common";
import { TeamService } from "./team.service";


@Controller('teams')
export class TeamController {
   constructor(private readonly teamService: TeamService) {   
    }

    @Get('/:id')
    async getTeam(@Res() response, @Param('id') teamId: string) {
        const team = await this.teamService.findTeam(teamId)
        return response.status(HttpStatus.OK).json({team});
    }
}
