import { Connection, Model } from 'mongoose';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Team, TeamDocument } from './team.schema';

@Injectable()
export class TeamService {
  constructor(
    @InjectModel(Team.name) private teamModel: Model<TeamDocument>,
    @InjectConnection() private connection: Connection
    ) {}

  

  async findAllTeam(): Promise<Team[]> {
    try {
      return this.teamModel.find()
    } catch (error) {
      console.log(error)
    }
    
  }

  async findTeam(id: string): Promise<Team> {
    const existingTeam = this.teamModel.findById(id).populate('players')
    if (!existingTeam) {
      throw new NotFoundException(`Team #${id} not found`)
    }
    return existingTeam
  }
}
