import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import { Schema as SchemaMongoose, HydratedDocument } from 'mongoose'
import { ApiProperty } from "@nestjs/swagger";
import { Player } from "src/player/player.schema";


export type TeamDocument = HydratedDocument<Team>;

@Schema()
export class Team {
   @ApiProperty()
   @Prop({unique: true})
   name: string;

   @ApiProperty()
   @Prop({unique: true})
   thumbnail: string;

   @ApiProperty()
   @Prop({type: [{ type: SchemaMongoose.Types.ObjectId, ref: 'Player' }]})
   players: Player[];
}
export const TeamSchema = SchemaFactory.createForClass(Team);