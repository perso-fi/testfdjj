import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import { Schema as SchemaMongoose, HydratedDocument } from 'mongoose'
import { ApiProperty } from "@nestjs/swagger";
import { Signin } from "./player.model";


export type PlayerDocument = HydratedDocument<Player>;

@Schema()
export class Player {
   @ApiProperty()
   @Prop({unique: true})
   name: string;

   @ApiProperty()
   @Prop({unique: true})
   thumbnail: string;

   @ApiProperty()
   @Prop()
   position: string;

//    @ApiProperty()
//    @Prop({type: })
//    signin: Signin;

   @ApiProperty()
   @Prop()
   born: Date;
}
export const PlayerSchema = SchemaFactory.createForClass(Player);