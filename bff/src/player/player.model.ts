export enum Currency {
    EUR = "eur"
}

export interface Signin {
    amount: number,
    currency: Currency
}