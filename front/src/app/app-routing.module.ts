import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessDeniedErrorComponent } from './core/access-denied-error/access-denied-error.component';
import { ServerErrorComponent } from './core/server-error/server-error.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'league',
    pathMatch: 'full'
  },
  {
    path: 'serverError',
    component: ServerErrorComponent
  },
  {
    path: 'accessDenied',
    component: AccessDeniedErrorComponent
  },
  {
    path: 'league',
    loadChildren: () => import('./league/league.module').then(m => m.LeagueModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}
