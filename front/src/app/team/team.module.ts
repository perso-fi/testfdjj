import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamComponent } from './team.component';
import { TeamRoutingModule } from './team-router.module';
import { TeamCardComponent } from './components/team-card/team-card.component';



@NgModule({
  declarations: [
    TeamComponent,
    TeamCardComponent
  ],
  imports: [
    CommonModule,
    TeamRoutingModule
  ]
})
export class TeamModule { }
