import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamComponent } from './team.component';
const routes: Routes = [
  {
    path: '',
    // canActivate: [RouteGuardsService],
    component: TeamComponent
  },
  {
    path: ':id',
    loadChildren: () => import('../player/player.module').then(m => m.PlayerModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
