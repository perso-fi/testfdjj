import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { League } from '../league/league.model';
import { LeagueService } from '../league/league.service';
import { TeamService } from './team.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent {

  @Input() league?: League

  constructor(private service: TeamService, private leagueService: LeagueService, private router: Router) {
    this.leagueService.league$.subscribe(league => this.league = league)
    this.leagueService.leagues$.subscribe(leagues => {
      this.leagueService.retrieveLeague(decodeURI(router.url.split('/')[router.url.split('/').length-1]))
    })
  }
}
