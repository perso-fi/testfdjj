import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, filter, Observable } from 'rxjs';
import { Team } from '../team/team.model';
import { ApiService } from '../api.service';


@Injectable({
  providedIn: 'root'
})
export class TeamService {
    
  private _team$: BehaviorSubject<Team> = new BehaviorSubject(undefined as unknown as Team)

  setTeam(value: Team) {
    this._team$?.next(value)
  }

  get team$(): Observable<Team> {
    return this._team$?.pipe(
      filter(item => item !== undefined)
    )
  }

  constructor(private apiService: ApiService) {

  }

  retrieveTeam(teamId: string) {
    this.apiService.getTeam(teamId).subscribe(({team}) => this.setTeam(team))
  }
}
