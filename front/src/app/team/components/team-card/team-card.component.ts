import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Team } from '../../team.model';

@Component({
  selector: 'app-team-card',
  templateUrl: './team-card.component.html',
  styleUrls: ['./team-card.component.css']
})
export class TeamCardComponent {
  @Input()
  team?: Team

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {

  }

  onClick() {
   this.router.navigate([`./${this.team?._id}`], {relativeTo: this.activatedRoute})
  }
}
