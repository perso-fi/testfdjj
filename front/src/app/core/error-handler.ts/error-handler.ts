import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core'
import { Router } from '@angular/router'
import { HttpErrorResponse } from '@angular/common/http'

@Injectable()
export class ErrorsHandler implements ErrorHandler {

  constructor(
    private injector: Injector,
    private router: Router,
  ) {}

  private readonly REDIRECTED_URL: string = 'redirectedUrl';
  
  handleError(error: Error | HttpErrorResponse) {
    const routerService = this.injector.get(Router)
    const ngZone = this.injector.get(NgZone);

    ngZone.run(() => {
      if (error instanceof HttpErrorResponse) {
        // Server or connection error
        if (!navigator.onLine) {
          // offline error
          console.error('Offline Error: ', error)
        }
        else if (error.status === 401) {
        //   this.authService.reconnect();
          console.error('HTTP Error: ', error)
        }
        else if (error.status === 500) {
          this.router.navigate(['serverError'])
          console.error('HTTP Error: ', error)
        }
        else if (error.status === 404) {
          this.router.navigate(['accessDenied'])
          console.error('HTTP Error: ', error)
        }
        else if (error.status === 403) {
          this.router.navigate(['accessDenied'])
          console.error('HTTP Error: ', error)
        }
        else {
          console.error('HTTP Error: ', error)
        }
      } else {
        // Client Error (Angular Error, ReferenceError...)
        console.error('Client Error: ', error)
        // routerService.navigate(['/error'])
      }
      console.error('Error: ', error)
    })
  }

}
