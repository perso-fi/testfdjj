import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorsComponent } from './errors.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ServerErrorsInterceptor } from './server-error-interceptor/server-error.interceptor';
import { ErrorsHandler } from '../error-handler.ts/error-handler';
import { AccessDeniedErrorComponent } from '../access-denied-error/access-denied-error.component';
import { ServerErrorComponent } from '../server-error/server-error.component';



@NgModule({
  declarations: [
    ErrorsComponent,
    AccessDeniedErrorComponent,
    ServerErrorComponent,

  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: ErrorsHandler,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorsInterceptor,
      multi: true,
    },
  ],
  imports: [
    CommonModule
  ]
})
export class ErrorsModule { }
