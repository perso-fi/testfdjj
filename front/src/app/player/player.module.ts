import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerComponent } from './player.component';
import { PlayerRoutingModule } from './player-router.module';
import { PlayerCardComponent } from './components/player-card/player-card.component';



@NgModule({
  declarations: [
    PlayerComponent,
    PlayerCardComponent
  ],
  imports: [
    CommonModule,
    PlayerRoutingModule
  ]
})
export class PlayerModule { }
