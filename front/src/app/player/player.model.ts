

export interface Player {
    name: string,
    thumbnail: string,
    position: string,
    signin: Signin,
    born: string
}

export enum Currency {
    EUR = "eur"
}


export interface Signin {
    currency: Currency,
    amount: number
}