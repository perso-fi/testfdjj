import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlayerComponent } from './player.component';
const routes: Routes = [
  {
    path: '',
    // canActivate: [RouteGuardsService],
    component: PlayerComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PlayerRoutingModule { }
