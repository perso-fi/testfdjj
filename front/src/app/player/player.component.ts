import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Team } from '../team/team.model';
import { TeamService } from '../team/team.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent {
  @Input()
  team?: Team

  constructor(private teamService: TeamService, private router: Router) {
    teamService.team$.subscribe(team => {
      this.team = team
      console.log(this.team)
    })
    teamService?.retrieveTeam(decodeURI(router.url.split('/')[router.url.split('/').length-1]))
  }
}
