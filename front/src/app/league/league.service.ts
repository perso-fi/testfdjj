import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, filter, Observable } from 'rxjs';
import { League } from './league.model';
import { ApiService } from '../api.service';
import { Team } from '../team/team.model';


@Injectable({
  providedIn: 'root'
})
export class LeagueService {

  private _leagues$: BehaviorSubject<League[]> = new BehaviorSubject([] as League[])
  
  setleagues(value: League[]) {
    this._leagues$?.next(value)
  }
  
  get leagues$(): Observable<League[]> {
    return this._leagues$?.pipe(
      filter(item => item !== undefined)
      )
  }

  private _league$: BehaviorSubject<League> = new BehaviorSubject(undefined as unknown as League)
  
  setLeague(value: League) {
    this._league$?.next(value)
  }
  
  get league$(): Observable<League> {
    return this._league$?.pipe(
      filter(item => item !== undefined)
      )
  }

  retrieveLeague(name: string) {
    const leagues = this._leagues$.getValue() || []
    const league = leagues.reduce((prev: League|undefined, crt) => crt.name === name ? crt : prev, undefined)
    if (league) {
      this.setLeague(league)
    }
  }
    
  constructor(private httpService:ApiService ) { 
    this.httpService.getAllLeague().subscribe(({leagues}) => this.setleagues(leagues))
  }
}
