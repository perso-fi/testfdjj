import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeagueComponent } from './league.component';
import { LeagueRoutingModule } from './league-routing.module';
import { LeagueContentComponent } from './league-content/league-content.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {OverlayModule} from '@angular/cdk/overlay';

@NgModule({
  declarations: [
    LeagueComponent,
    LeagueContentComponent
  ],
  imports: [
    CommonModule,
    MatAutocompleteModule,
    OverlayModule,
    MatInputModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    LeagueRoutingModule
  ]
})
export class LeagueModule { }
