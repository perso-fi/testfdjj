import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ActivatedRoute, Router } from '@angular/router';
import { map, Observable, of, startWith } from 'rxjs';
import { ApiService } from '../api.service';
import { Team } from '../team/team.model';
import { TeamService } from '../team/team.service';
import { League } from './league.model';
import { LeagueService } from './league.service';

@Component({
  selector: 'app-league',
  templateUrl: './league.component.html',
  styleUrls: ['./league.component.css']
})
export class LeagueComponent {

  options: League[] = []
  control = new FormControl<League|string>('')
  filteredOptions?: Observable<League[]>


  _leagues: League[] = []
  @Input() 
  get leagues(): League[] {
    return this._leagues
  }

  set leagues(value: League[]) {
    this._leagues = this.options = value
  }

  constructor(private service: LeagueService, private teamService: TeamService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.service.leagues$.subscribe(
      leagues => this.leagues = leagues
    )
  }

  ngOnInit() {
    this.filteredOptions = this.control.valueChanges.pipe(
      startWith(''),
      map(value => {
        const name = typeof value === 'string' ? value : value?.name;
        return name ? this._filter(name as string) : this.options.slice();
      }),
    );
  }

  private _filter(value: string): League[] {
    const filterValue = this._normalizeValue(value);
    return this.options.filter(option => this._normalizeValue(option.name||"").includes(filterValue));
  }

  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  displayFn(league: League): string {
    return league && league.name ? league.name : '';
  }

  onSelect(event: MatAutocompleteSelectedEvent) {
    this.router.navigate([`./${event.option.value.name}`], {relativeTo: this.activatedRoute})
  }
}
