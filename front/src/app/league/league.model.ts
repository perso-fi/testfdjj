import { Team } from "../team/team.model";

export interface League {
    name: string,
    sport: string,
    teams: Team[]
}