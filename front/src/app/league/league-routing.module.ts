import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LeagueContentComponent } from './league-content/league-content.component';
import { LeagueComponent } from './league.component';

const routes: Routes = [
  {
    path: '',
    // canActivate: [RouteGuardsService],
    component: LeagueComponent
  },
  {
    path: ':id',
    loadChildren: () => import('../team/team.module').then(m => m.TeamModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class LeagueRoutingModule { }
