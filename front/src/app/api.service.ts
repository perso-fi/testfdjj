import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, filter, Observable } from 'rxjs';
import { League } from './league/league.model';
import { Team } from './team/team.model';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient ) { 
    
  }

  getAllLeague(): Observable<{leagues: League[]}> {
    return this.http.get<{leagues: League[]}>('/api/leagues')
  }

  getTeam(teamId: string) {
    return this.http.get<{team: Team}>(`/api/teams/${teamId}`)
  }
}
