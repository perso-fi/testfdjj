FROM node:16-buster-slim as front
WORKDIR /usr/src
RUN npm i -g @angular/cli
COPY ./front /usr/src/
RUN yarn

FROM node:16-buster-slim as bff
WORKDIR /usr/src
RUN npm i -g @nestjs/cli
COPY ./bff /usr/src/
RUN yarn

FROM mongo as mongo
RUN apt-get update && apt-get install -y unzip
COPY ./infra/mongo/sports.zip /mongo-seed/sports.zip
RUN ls -la /mongo-seed
RUN unzip -j /mongo-seed/sports.zip -d /mongo-seed/
