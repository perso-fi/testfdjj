#! /bin/bash

# apt-get update && apt-get install -y unzip
# echo "$PWD"
# unzip -j mongo-seed/sports.zip -d mongo-seed/

# mongorestore -d sports mongo-seed/
# mongoimport --host mongo -d sports -c leagues mongo-seed/leagues.bson

# mongoimport --host mongo-fdj --authenticationDatabase admin --username ${MONGO_INITDB_ROOT_USERNAME} --password ${MONGO_INITDB_ROOT_PASSWORD} -d ${MONGO_INITDB_DATABASE} -c leagues --type=json --file /mongo-seed/leagues.metadata.json
# mongoimport --host mongo-fdj --authenticationDatabase admin --username ${MONGO_INITDB_ROOT_USERNAME} --password ${MONGO_INITDB_ROOT_PASSWORD} -d ${MONGO_INITDB_DATABASE} -c players --type=json --file /mongo-seed/players.metadata.json
# mongoimport --host mongo-fdj --authenticationDatabase admin --username ${MONGO_INITDB_ROOT_USERNAME} --password ${MONGO_INITDB_ROOT_PASSWORD} -d ${MONGO_INITDB_DATABASE} -c teams --type=json --file /mongo-seed/teams.metadata.json

mongorestore --host mongo-fdj --authenticationDatabase admin --username ${MONGO_INITDB_ROOT_USERNAME} --password ${MONGO_INITDB_ROOT_PASSWORD} -d ${MONGO_INITDB_DATABASE} -c teams /mongo-seed/teams.bson
mongorestore --host mongo-fdj --authenticationDatabase admin --username ${MONGO_INITDB_ROOT_USERNAME} --password ${MONGO_INITDB_ROOT_PASSWORD} -d ${MONGO_INITDB_DATABASE} -c players /mongo-seed/players.bson
mongorestore --host mongo-fdj --authenticationDatabase admin --username ${MONGO_INITDB_ROOT_USERNAME} --password ${MONGO_INITDB_ROOT_PASSWORD} -d ${MONGO_INITDB_DATABASE} -c leagues /mongo-seed/leagues.bson