# Test
## Mise en place de la stack
Toute la stack est dans des containers : 
    
* container qui embarque l'application Front Angular
* container qui embarque l'application Node en NESTJS
* Dans l'infra on a tous les autres containers : 
     * Traefik en tant que reverse-proxy
     * Mongodb pour la DB
     * Mongodb-express pour l'analyse des données
     * Mongo-seed pour la migration initiale

## Lancement de la stack

```
docker compose up
```
## Affichage en local

Traefik est configuré pour utiliser le endpoint docker.localhost.

LE front est donc accessible via 
```
http://docker.localhost
```
et le back : 
```
docker.localhost/api
```
